<?php

namespace App\Admin\Controllers;

use App\Models\Division;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

// 10月22日追記
use Illuminate\Http\Request;
use App\Task;
use Excel;


class DivisionController extends Controller
{
    use HasResourceActions;

    //」 10/22追記
    protected $task = null;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['tasks'] = $this->task->all();
        return view('index', $data);
    }


    /**
     * CSVインポート
     *
     * @param  Request
     * @return \Illuminate\Http\Response
     */
     public function import(Request $request)
     {
         $file = $request->file('csv_file');
         $reader = Excel::load($file->getRealPath());

         $rows = $reader->toArray();

         foreach ($rows as $row){
             if (!isset($row[''])) {
                 return redirect()->back();
             }

             $record = $this->task->firstOrNew(['division' => $row['division']]);
             $record->name = $row['division'];
             $record->save();
         }
         return redirect()->action('DivisionController@index');
     }
     // 10月22日追記ここまで


    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed   $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed   $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = Admin::grid(Division::class, function (Grid $grid) {
        //$grid = new Grid(new Division);

        $grid->id('事業部ID')->sortable();
        $grid->division('事業部名')->sortable();
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');
        $grid->actions(function ($actions) {
        // $actions->disableDelete();
        // $actions->disableEdit();
        $actions->disableView();
        });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Division::findOrFail($id));

        $show->id('事業部ID');
        $show->division('事業部名');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Division::class, function (Form $form){
        //$form = new Form(new Division);

        $form->display('id', '事業部ID');
        $form->text('division', '事業部名');
        $form->display('created_at', 'Created at');

        //return $form;
        });
    }
}
